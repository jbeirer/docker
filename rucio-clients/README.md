ATLAS RUCIO CLIENTS
===================

This configuration can be used to build the rucio clients for ATLAS. It is
using the base rucio clients image from 
[rucio/rucio-clients](https://hub.docker.com/r/rucio/rucio-clients) and
adds the ATLAS-specific configuration and certificates.

You can build it like:

```bash
docker build -f Dockerfile --build-arg TAG=1.22.8_py3 -t atlas/rucio-clients:default .
```

Examples
--------

You can find some pre-built images on
[atlas/rucio-clients](https://hub.docker.com/r/atlas/rucio-clients).
