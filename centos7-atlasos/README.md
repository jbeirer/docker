CC7 ATLAS OS
=============

This configuration can be used to set up a base image that ATLAS analysis
and offline releases can be installed on top of. So that each image would
not have to duplicate the same base components.

During the build you can, and should choose the GCC version to be installed,
explicitly.

You can build a GCC 8 based image with the following:

```bash
docker build -t atlas/centos7-atlasos:X.Y.Z-gcc8 -t atlas/centos7-atlasos:latest-gcc8 \
   --build-arg GCCVERSION=8.3.0 \
   --build-arg GCCPACKAGES="gcc_8.3.0_x86_64_centos7 binutils_2.30_x86_64_centos7" \
   --compress --squash .
```

For building a GCC 11 based image, use something like:

```bash
docker build -t atlas/centos7-atlasos:X.Y.Z-gcc11 -t atlas/centos7-atlasos:latest-gcc11 \
   -t atlas/centos7-atlasos:latest \
   --build-arg GCCVERSION=11.2.0 \
   --build-arg GCCPACKAGES="gcc_11.2.0_x86_64_centos7 binutils_2.37_x86_64_centos7" \
   --compress --squash .
```

Images
------

You can find pre-built images on
[atlas/centos7-atlasos](https://hub.docker.com/r/atlas/centos7-atlasos/).
